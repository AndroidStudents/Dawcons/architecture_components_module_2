package com.dwtraining.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dwtraining.interfaces.MoviesAPIService
import com.dwtraining.models.Movie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Giovani González
 * Created by Giovani on 2019-06-22.
 */
class MoviesViewModel : ViewModel() {

    private val TAG = MoviesViewModel::class.java.simpleName
    private lateinit var movies: MutableLiveData<List<Movie>>
    var isRefreshing: MutableLiveData<Boolean> = MutableLiveData()

    init {
        getMovies()
    }

    @JvmOverloads
    fun getMovies(reloadAgain: Boolean = false): LiveData<List<Movie>> {
        if (!::movies.isInitialized) {
            movies = MutableLiveData()
            getMoviesFromAPI()
        }
        if (reloadAgain) {
            getMoviesFromAPI()
        }
        return movies
    }

    private fun getMoviesFromAPI() {
        // Here is the code to get data from API.
        isRefreshing.postValue(true)
        val call = MoviesAPIService.create().getAllMovies()
        call.enqueue(object : Callback<List<Movie>> {
            override fun onFailure(call: Call<List<Movie>>, t: Throwable) {
                Log.e(TAG, "Something went wrong, $t")
                isRefreshing.postValue(false)
            }

            override fun onResponse(call: Call<List<Movie>>, response: Response<List<Movie>>) {
                val listMovies = response.body()
                listMovies?.let {
                    Log.d(TAG, it.toString())
                    movies.postValue(it)
                    isRefreshing.postValue(false)
                }
            }
        })
    }
}
package com.dwtraining.interfaces

import com.dwtraining.models.Movie
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

/**
 * @author Giovani González
 * Created by Giovani on 2019-06-23.
 */

const val BASE_URL = "https://android-course-api.herokuapp.com/"

interface MoviesAPIService {

    @GET("api/movies")
    fun getAllMovies() : Call<List<Movie>>

    companion object {
        fun create() : MoviesAPIService {
            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .build()
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build()
            return retrofit.create(MoviesAPIService::class.java)
        }
    }
}